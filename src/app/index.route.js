(function() {
  'use strict';

  angular
    .module('qaApp')
    .config(routerConfig);

  /** @ngInject */

  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
          templateUrl: 'app/main/main.html',
          controller: 'MainController',
          controllerAs: 'vm'
      })
      .state('main.questions', {
          url: '/',
          templateUrl: 'app/questions/questions-list.view.html',
          controller: 'QuestionsListController',
          controllerAs: 'vm'
      })
      .state('main.question-details', {
          url: '/question-details?id',
          templateUrl: 'app/questions/question-details/question-details.view.html',
          controller: 'QuestionDetailsController',
          controllerAs: 'vm'
      })
      .state('main.answered-questions', {
          url: '/answered-questions',
          templateUrl: 'app/questions/answered-questions/answered-questions.view.html',
          controller: 'AnsweredQuestionsController',
          controllerAs: 'vm'
      })
      .state('main.questions-without-answers', {
          url: '/questions-without-answers',
          templateUrl: 'app/questions/questions-without-answers/questions-without-answers.view.html',
          controller: 'QuestionsWithoutAnswersController',
          controllerAs: 'vm'
      })
      .state('main.add-question', {
          url: '/add-question',
          templateUrl: 'app/questions/add-question/add-question.view.html',
          controller: 'AddQuestionController',
          controllerAs: 'vm'
      })

    $urlRouterProvider.otherwise('/');
  }

})();
