/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('qaApp')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
