(function() {
    'use strict';

    angular
        .module('qaApp')
        .controller('AddQuestionController', AddQuestionController);

    AddQuestionController.$inject = ['$scope', '$rootScope', '$state', '$window', 'indexedDBDataQa'];

    function AddQuestionController($scope, $rootScope, $state, $window, indexedDBDataQa) {

    	var vm = this;
            vm.save = save;
            vm.newQuestion;

        function save () {

            vm.newQuestion = {
                title: vm.newQuestion.title,
                description: vm.newQuestion.description,
                autor: vm.newQuestion.autor,
                date: new Date()
            };

            indexedDBDataQa.addQuestion(vm.newQuestion)
                .then(function () {
            console.log("work");

                    $state.go("main.questions");
                }, function (err) {
                    $window.alert(err);
                });

        }

    }

})();