(function() {
  	'use strict';

	angular
	    .module('qaApp')
		.controller('QuestionsWithoutAnswersController', QuestionsWithoutAnswersController);

	QuestionsWithoutAnswersController.$inject = ['$rootScope', '$window', 'indexedDBDataQa', '$scope'];

	/** @ngInject */
	function QuestionsWithoutAnswersController($rootScope, $window, indexedDBDataQa, $scope) {
		

		var vm = this;
	        vm.refreshList = refreshList;
	        vm.questions = [];
	    refreshList();

	    function refreshList () {

	      	indexedDBDataQa.getQuestionWithoutAnswers().then(function (data) {
	        	vm.questions = data;
	      	}, function (err) {
	        	$window.alert(err);
	      	});

	    }

	}

})();