(function() {
'use strict';

	angular
		.module('qaApp')
		.controller('AnsweredQuestionsController', AnsweredQuestionsController);

	AnsweredQuestionsController.$inject = ['$rootScope', '$window', 'indexedDBDataQa'];

	/** @ngInject */
	function AnsweredQuestionsController($rootScope, $window, indexedDBDataQa) {

		var vm = this;
	        vm.refreshList = refreshList;
	        vm.questions = [];

	    refreshList();

	    function refreshList () {

	      	indexedDBDataQa.getAnsweredQuestion().then(function (data) {
	        	vm.questions = data;
	      	}, function (err) {
	        	$window.alert(err);
	      	});

	    }

	}

}) ();