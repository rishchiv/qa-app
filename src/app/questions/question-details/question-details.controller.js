(function() {
  	'use strict';

  	angular
    	.module('qaApp')
    	.controller('QuestionDetailsController', QuestionDetailsController);

  	QuestionDetailsController.$inject = ['$rootScope', '$window', 'indexedDBDataQa', '$stateParams'];

  	/** @ngInject */

  	function QuestionDetailsController($rootScope, $window, indexedDBDataQa, $stateParams) {
    
	    var vm = this;
	        vm.refreshList = refreshList;
	        vm.postAnswer = postAnswer;
	        vm.answers = [];
	    	  vm.newAnswer;

	    refreshList();

		function refreshList () {

			indexedDBDataQa.getQuestionById($stateParams.id)

			.then(function (data) {
				vm.question = data;
				vm.answers = data.answers;
			}, function (err) {
				$window.alert(err);
			});

		}

    function postAnswer () {

    	vm.newAnswer = {
    		autor: vm.newAnswer.autor,
    		text: vm.newAnswer.text,
    		date: new Date()
    	}

      indexedDBDataQa.addAnswer(vm.newAnswer, $stateParams.id)
      .then(function () {
          refreshList();
      }, function (err) {
          $window.alert(err);
      });
      
    }
   
  }

})();