(function() {
  'use strict';

  angular
    .module('qaApp')
    .controller('QuestionsListController', QuestionsListController);

  QuestionsListController.$inject = ['$rootScope', '$window', 'indexedDBDataQa'];

  /** @ngInject */
  function QuestionsListController($rootScope, $window, indexedDBDataQa) {
    
    var vm = this;
        vm.refreshList = refreshList;
        vm.questions = [];

    refreshList();

    function refreshList () {

      indexedDBDataQa.getQuestions().then(function (data) {
        vm.questions = data;
      }, function (err) {
        $window.alert(err);
      });

    }
  
  }

})();