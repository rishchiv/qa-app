(function() {
  'use strict';

  angular
    .module('qaApp')
    .directive('qaHeader', qaHeader);

  /** @ngInject */
  function qaHeader() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'app/components/header/header.view.html',
      scope: false,
      controller: HeaderController,
      controllerAs: 'vm',
      bindToController: false
    };

    return directive;

  }

  HeaderController.$inject = ['$scope'];

  /** @ngInject */
  function HeaderController($scope) {
    
    var vm = this;
        vm.isCollapsed = true;

  }

})();
