(function() {
  'use strict';

  	angular
		.module('qaApp')
		.factory('indexedDBDataQa', indexedDBDataQa);

  	indexedDBDataQa.$inject = ['$window', '$q'];

  	function indexedDBDataQa($window, $q) {
  		$window.IDBKeyRange = $window.IDBKeyRange || $window.webkitIDBKeyRange || $window.msIDBKeyRange;
  		$window.IDBTransaction = $window.IDBTransaction || $window.webkitIDBTransaction || $window.msIDBTransaction;

	    var indexedDB = $window.indexedDB || $window.mozIndexedDB || $window.webkitIndexedDB || $window.msIndexedDB,
	    	db = null,
	    	lastIndex = 0,
	    	dbDeffered = $q.defer(),
	    	dbPromise = dbDeffered.promise,
	    	service = {};

	    	service.openDB = openDB;
	    	service.getQuestions = getQuestions;
	    	service.getQuestionById = getQuestionById;
	    	service.addQuestion = addQuestion;
	    	service.addAnswer = addAnswer;
	    	service.getQuestionWithoutAnswers = getQuestionWithoutAnswers;
	    	service.getAnsweredQuestion = getAnsweredQuestion;
			
			openDB(); // initialize connection to database

    		return service;



    	function openDB () {

	        var version = 1;
	        var request = indexedDB.open("questionData", version);

	        request.onupgradeneeded = function (e) {
	            var database = e.target.result;
	            e.target.transaction.onerror = indexedDB.onerror;

	            if (database.objectStoreNames.contains("questions")) {
	                database.deleteObjectStore("questions");
	            }

	            var store = database.createObjectStore("questions", {keyPath: 'id'});
	            	store.createIndex('id', 'id', { unique: true });

	        };

	        request.onsuccess = function (e) {
	        	dbDeffered.resolve();
	        	db = e.target.result;
	            console.log('DB opened successfully', e);
	        };

	        request.onerror = function (e) {
	        	dbDeffered.reject();
	            console.log('DB open error', e);
	        };
		        
		};


	 	function getQuestions () {

	        var deferred = $q.defer();

        	dbPromise.then(function(){

        		var trans = db.transaction(["questions"], "readonly");
	            var store = trans.objectStore("questions");
	            var questions = [];

	            // Get everything in the store;
	            var keyRange = $window.IDBKeyRange.lowerBound(0);
	            var cursorRequest = store.openCursor(keyRange);

	            cursorRequest.onsuccess = function (e) {
	                var result = e.target.result;
	                if (result === null || result === undefined) {
	                    deferred.resolve(questions);
	                } else {
	                    questions.push(result.value);
	                    if (result.value.id > lastIndex) {
	                        lastIndex = result.value.id;
	                    }
	                    result.
	                    continue ();
	                }
	            };

	            cursorRequest.onerror = function (e) {
	                console.log(e.value);
	                deferred.reject("Something went wrong!!!");
	            };

        	});

	        return deferred.promise;
	 	}

	 	function getQuestionById (id) {

	        var deferred = $q.defer();

	        dbPromise.then(function(){

	        	var trans = db.transaction(["questions"], "readonly");
	            var store = trans.objectStore("questions");
	            var answer = {};

				var myIndex = store.index('id'); 
				var request = myIndex.get(Number(id));

				request.onsuccess = function(e){
					console.log('e', e);
					var data = request.result;
					deferred.resolve(data);
				}

				request.onerror = function (e) {
	                console.log(e.value);
	                deferred.reject("Cannot get question");
	            };

	        });

	        return deferred.promise;
	 		
	 	}

	 	function getQuestionWithoutAnswers () {

	        var deferred = $q.defer();

			dbPromise.then(function(){

	            var trans = db.transaction(["questions"], "readonly");
	            var store = trans.objectStore("questions");
	            var questions = [];

	            // Get everything in the store;
	            var keyRange = $window.IDBKeyRange.lowerBound(0);
	            var cursorRequest = store.openCursor(keyRange);

	            cursorRequest.onsuccess = function (e) {
	                var result = e.target.result;
	                if (result === null || result === undefined) {
	                    deferred.resolve(questions);
	                } else {
	                    if (result.value.answers.length == 0) {
	                    	questions.push(result.value);
	                    }
	                    result.
	                    continue ();
	                }
	            };

	            cursorRequest.onerror = function (e) {
	                console.log(e.value);
	                deferred.reject("Something went wrong!!!");
	            };
	        });

	        return deferred.promise;
	 		
	 	}

	 	function getAnsweredQuestion () {

	        var deferred = $q.defer();

			dbPromise.then(function(){  

	            var trans = db.transaction(["questions"], "readonly");
	            var store = trans.objectStore("questions");
	            var questions = [];

	            // Get everything in the store;
	            var keyRange = $window.IDBKeyRange.lowerBound(0);
	            var cursorRequest = store.openCursor(keyRange);

	            cursorRequest.onsuccess = function (e) {
	                var result = e.target.result;
	                if (result === null || result === undefined) {
	                    deferred.resolve(questions);
	                } else {
	                    if (result.value.answers.length > 0) {
	                    	questions.push(result.value);
	                    }
	                    result.
	                    continue ();
	                }
	            };

	            cursorRequest.onerror = function (e) {
	                console.log(e.value);
	                deferred.reject("Something went wrong!!!");
	            };
	        });

	        return deferred.promise;
	 		
	 	}

	    function addQuestion (question) {

	        var deferred = $q.defer();

			dbPromise.then(function(res){

	            var trans = db.transaction(["questions"], "readwrite");
	            var store = trans.objectStore("questions");
	            
	            lastIndex++;

	            var request = store.add({
	            	"id": lastIndex,
	                "title": question.title,
	                "description": question.description,
	                "autor": question.autor,
	                "date": question.date,
	                "answers": []
	            });

	            request.onsuccess = function (e) {
	                deferred.resolve();
	            };

	            request.onerror = function (e) {
	                console.log(e.value);
	                deferred.reject("Question couldn't be added!");
	            };

	        });

	        return deferred.promise;

	    }

	    function addAnswer (answer, id) {

			var deferred = $q.defer();

			dbPromise.then(function(res){

	        	var trans = db.transaction(["questions"], "readwrite");
	            var store = trans.objectStore("questions");
	            
	            var myIndex = store.index('id'); 
				var request = myIndex.get(Number(id));

				request.onsuccess = function(e) {

				    var data = request.result;
				    	data.answers.push(answer);

				  	var requestUpdate = store.put(data);

				    requestUpdate.onsuccess = function(e) {
						deferred.resolve(e);
				    };
				  	requestUpdate.onerror = function(e) {
						console.log(e.value);
						deferred.reject("Question couldn't be added!");
				   	};
				};

	        });

	        return deferred.promise;

	    }

  	}

}) ();